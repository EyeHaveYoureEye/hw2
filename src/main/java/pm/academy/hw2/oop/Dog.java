package pm.academy.hw2.oop;

public class Dog implements Animal {
  private String name;

  public Dog(String name, int weight) {
    this.name = name;
  }

  @Override
  public String getAnimalType() {
    return null;
  }

  @Override
  public String getName() {
    return null;
  }

  @Override
  public int getWeight() {
    return 0;
  }
}
